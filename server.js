const express = require("express");

// mongoose is a package that allows us to create schemas to model our data structure and to manipulate our database using different access methods. 
const mongoose = require("mongoose");

const port = 3001;
const app = express();

// [SECTION] MongoDB connection
	// Syntax: 
		// mongoose.connect("mongoDBconnectionString", {options to avoid errors in our connection});

mongoose.connect("mongodb+srv://admin:admin@b245-dunglao.tbtmdrd.mongodb.net/s35-discussion?retryWrites=true&w=majority", 
{
	// allows us to avoid any current and future errors while connecting to mongoDB. 
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;

// error handling in connecting 

db.on("error", console.error.bind(console, "Connection error"));

// this will be triggered if the connection is successful. 
db.once("open", ()=> console.log("We're connected to the cloud database!"));

// Mongoose Schemas
/* 
	Schemas determine the structure of the documents to be written in the data base
	Schemas act as a blueprint to our database

	Syntax:
		const schemaName = new mongoose.Schema({keyvaluepairs});

	*/
	// taskSchema it contains two properties: name & status
	// required is used to specifiy that a field must not be empty. 
	// default- is used if a field value is not supplied. 

	const taskSchema = new mongoose.Schema({
		name: {
			type: String,
			required: [true, "task name is required!"]
		},
		status: {
			type: String,
			default: "pending"
		}

	})

	

/*
[SECTION] Models
	Uses schema to create.instantiate documents/objects that follows our schema structure. 
	
	the variable/object that will be created can be used to run commands with our database

	Syntax: 
		const variableName = mongoose.model("collectionName", schemaName);

	*/

	const Task = mongoose.model("Task", taskSchema);



// middlewares

app.use(express.json()) // Allows the app to read JSON data
app.use(express.urlencoded({extended:true})) // it will allow our app to read data from forms. 

/*
[SECTION] Routing
	 Create/add new task
		 1. Check if the task is existing. 
			- if task already exist in the database, we will return a message "The task is already existing!"
			- if the task doesn't exist in the database, we will add it in the database. 
			*/

app.post("/tasks", (request, response) => {
	let input = request.body;
		console.log(input.status);
		if(input.status === undefined){
			Task.findOne({name: input.name},(error, result) =>{
				console.log(result);
				if(result !== null){
					return response.send("The task is already existing");
				}else{
					let newTask = new Task({
						name: input.name,
						})

					// save() method will save the object in the collection that the object instatiated. 
					newTask.save((saveError, savedTask) => {
						if(saveError){
							return console.log(saveError);
						}else{
							return response.send("New Task Created!");
						}
					})
				}

			});
		}else{
			Task.findOne({name: input.name},(error, result) =>{
				console.log(result);
				if(result !== null){
					return response.send("The task is already existing");
				}else{
					let newTask = new Task({
						name: input.name,
						status: input.status

						})

					// save() method will save the object in the collection that the object instatiated. 
					newTask.save((saveError, savedTask) => {
						if(saveError){
							return console.log(saveError);
						}else{
							return response.send("New Task Created!");
						}
					})
				}

			});

		}


})

/*
Task.findOne({name: input.name},(error, result) =>{
			console.log(result);
			if(result !== null){
				return response.send("The task is already existing");
			}else{
				let newTask = new Task({
					name: input.name,
					})

				// save() method will save the object in the collection that the object instatiated. 
				newTask.save((saveError, savedTask) => {
					if(saveError){
						return console.log(saveError);
					}else{
						return response.send("New Task Created!");
					}
				})
			}

		});

*/




// [SECTION] Retrieving all the tasks. 
	app.get("/tasks", (request, response) => {
		Task.find({}, (error,result) => {
			if(error){
				console.log(error);
			}else{
				return response.send(result);
			}
		})

	})


// ACTIVITY


	const userSchema = new mongoose.Schema({
		username: {
			type: String,
			required: [true, "Username and password cannot be empty."]
		},
		password: {
			type: String,
			required: [true, "Username and password cannot be empty."]
		}

	})

	const User = mongoose.model("User", userSchema);

	app.post("/signup", (request, response) => {
		let input = request.body;
		if(input.password === undefined){
			return response.send("Username and password cannot be empty.");

		}else{
			User.findOne({username: input.username},(error, result) =>{
				if(result !== null){
					return response.send("Username was already registered.");
				}else{
					let newUser = new User({
						username: input.username,
						password: input.password
						})
					newUser.save((saveError, savedTask) => {
						if(saveError){
							return console.log(saveError);
						}else{
							return response.send("New user registered.");
						}
					})
				}

			})};

	});

	app.get("/signup", (request, response) => {
		User.find({}, (error,result) => {
			if(error){
				console.log(error);
			}else{
				return response.send(result);
			}
		})

	})


app.listen(port, ()=> console.log(`Server is running at port ${port}!`));




